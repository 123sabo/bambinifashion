@extends('base')

@section('content')
    <p>Order data:</p>

    <label>Id: </label> {{ $order->id }} <br>
    <label>Name: </label> {{ $order->client_name }} <br>
    <label>Address: </label> {{ $order->client_address }} <br>
    <label>Shipping: </label> {{ $order->total_shipping_value }} <br>
    <label>Price: </label> {{ $order->total_product_value }} <br>

@endsection

