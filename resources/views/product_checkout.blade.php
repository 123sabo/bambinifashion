@extends('base')

@section('content')
    <p>Product checkout: {{ $product->name }}</p>

    <form action="/checkout/{{ $product->id }}" method="POST">
        @csrf
        <label>Name: </label><input type="text" name="name"><br>
        <label>Address: </label> <input type="text" name="address"><br>
        <label>Shipping option: </label>
        <div>
            <input type="radio" name="shipping" value="0"> Standard (Free)<br>
            <input type="radio" name="shipping" value="10"> Express (10 EUR)<br>
        </div>
        <label>Price: </label>{{ $product->price }} <br> <input type="hidden" name="price" value="{{ $product->price }}">

        <input id="card-holder-name" type="text">

        <!-- Stripe Elements Placeholder -->
        <div id="card-element"></div>

        <button id="card-button">
            Process Payment
        </button>

        <input type="submit">
    </form>

@endsection

@section('javascript')
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        const stripe = Stripe('stripe-public-key');

        const elements = stripe.elements();
        const cardElement = elements.create('card');

        cardElement.mount('#card-element');
    </script>
@endsection
