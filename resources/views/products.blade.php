@extends('base')

@section('content')
    <p>Products</p>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Brand</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->brand->name }}</td>
                    <td>{{ $product->price }}</td>
                    <th><a href="/checkout/{{ $product->id }}">Buy</a></th>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
