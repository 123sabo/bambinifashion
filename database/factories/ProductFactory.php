<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Product;
use App\Brand;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) use ($factory) {
    return [
        'name' => $faker->unique()->slug,
        'price' => $faker->randomFloat(2, 1, 5000),
    ];
});

