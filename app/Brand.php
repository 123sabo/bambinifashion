<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 * @package App
 */
class Brand extends Model
{
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
