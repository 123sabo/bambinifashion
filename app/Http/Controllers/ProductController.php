<?php


namespace App\Http\Controllers;


use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends BaseController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function products()
    {
        $products = Product::all();

        return view('products', ['products' => $products]);
    }

    /**
     * @param int $productId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkout(int $productId)
    {
        $product = Product::find($productId);

        return view('product_checkout', ['product' => $product]);
    }

    public function buy(int $productId, Request $request)
    {
        $order = new Order();

        $order->client_name = $request->get('name');
        $order->client_address = $request->get('address');
        $order->total_product_value = $request->get('price');
        $order->total_shipping_value = $request->get('shipping');

        $order->save();

        return redirect()->action('OrderController@get', ['orderId' => $order->id]);
    }
}
