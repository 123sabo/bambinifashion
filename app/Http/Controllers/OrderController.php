<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use App\Order;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends BaseController
{
    public function get($orderId)
    {
        $order = Order::find($orderId);

        return view('order', ['order' => $order]);
    }
}
