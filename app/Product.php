<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App
 */
class Product extends Model
{
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
