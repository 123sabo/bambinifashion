<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@products');

Route::get('/checkout/{productId}', 'ProductController@checkout');

Route::post('/checkout/{productId}', 'ProductController@buy');

Route::get('/order/{orderId}', 'OrderController@get');
